
// CONNECTIONS:
// DS1302 CLK/SCLK --> 5
// DS1302 DAT/IO --> 4
// DS1302 RST/CE --> 2
// DS1302 VCC --> 3.3v - 5v
// DS1302 GND --> GND
#include <Arduino.h>
#include <U8g2lib.h>
#include <SPI.h>
#include <Wire.h>

#include <ThreeWire.h>  
#include <RtcDS1302.h>
#include <OneWire.h>
OneWire ds(8);

U8G2_SSD1306_128X32_UNIVISION_F_HW_I2C u8g2(U8G2_R0); 

ThreeWire myWire(4,5,2); // IO, SCLK, CE
RtcDS1302<ThreeWire> Rtc(myWire);
float Temp;
bool show_dots = true;
void setup () 
{
    Serial.begin(57600);
    u8g2.begin();
    Serial.print("compiled: ");
    Serial.print(__DATE__);
    Serial.println(__TIME__);

    Rtc.Begin();

    RtcDateTime compiled = RtcDateTime(__DATE__, __TIME__);
    printDateTime(compiled);
    Serial.println();

    if (!Rtc.IsDateTimeValid()) 
    {
        // Common Causes:
        //    1) first time you ran and the device wasn't running yet
        //    2) the battery on the device is low or even missing

        Serial.println("RTC lost confidence in the DateTime!");
        Rtc.SetDateTime(compiled);
    }

    if (Rtc.GetIsWriteProtected())
    {
        Serial.println("RTC was write protected, enabling writing now");
        Rtc.SetIsWriteProtected(false);
    }

    if (!Rtc.GetIsRunning())
    {
        Serial.println("RTC was not actively running, starting now");
        Rtc.SetIsRunning(true);
    }

    RtcDateTime now = Rtc.GetDateTime();
    if (now < compiled) 
    {
        Serial.println("RTC is older than compile time!  (Updating DateTime)");
        Rtc.SetDateTime(compiled);
    }
    else if (now > compiled) 
    {
        Serial.println("RTC is newer than compile time. (this is expected)");
    }
    else if (now == compiled) 
    {
        Serial.println("RTC is the same as compile time! (not expected but all is fine)");
    }
}

void loop () 
{
    RtcDateTime now = Rtc.GetDateTime();
    byte t_data[2];
    ds.reset(); 
    ds.write(0xCC);
    ds.write(0x44);
    delay(15);
    ds.reset();
    ds.write(0xCC);
    ds.write(0xBE);
    t_data[0] = ds.read(); 
    t_data[1] = ds.read();
    int iTemp = (t_data[1]<< 8)+t_data[0];
    float t = (float(Temp) / 16.0);
    Temp = float(iTemp)/16.0;
    
    printDateTime(now);
    displayDateTime(now);
    
    Serial.println(t);

    if (!now.IsValid())
    {
        // Common Causes:
        //    1) the battery on the device is low or even missing and the power line was disconnected
        Serial.println("RTC lost confidence in the DateTime!");
    }
 
    show_dots = !show_dots;
    delay(1000); // ten seconds
}

#define countof(a) (sizeof(a) / sizeof(a[0]))

void printDateTime(const RtcDateTime& dt)
{
    char datestring[20];

    snprintf_P(datestring, 
            countof(datestring),
            PSTR("%02u/%02u/%04u %02u:%02u:%02u"),
            dt.Month(),
            dt.Day(),
            dt.Year(),
            dt.Hour(),
            dt.Minute(),
            dt.Second() );

}

void displayDateTime(const RtcDateTime& dt)
{
  char datestring[20];

  if(show_dots) 
      snprintf_P(datestring, 
              countof(datestring),
              PSTR("%02u:%02u:%02u"),
              dt.Hour(),
              dt.Minute(),
              dt.Second() );
   else
       snprintf_P(datestring, 
              countof(datestring),
              PSTR("%02u %02u %02u"),
              dt.Hour(),
              dt.Minute(),
              dt.Second() );
            
   u8g2.clearBuffer();          // clear the internal memory
   u8g2.setFont(u8g2_font_fur20_tn);  // choose a suitable font at https://github.com/olikraus/u8g2/wiki/fntlistall
   u8g2.drawStr(0,20,datestring);
   
   u8g2.setFont(u8g2_font_tenfatguys_tf);  // choose a suitable font at https://github.com/olikraus/u8g2/wiki/fntlistall

   char tempstr[5];
   dtostrf(abs(Temp),2, 1, tempstr);
   char outstr[6];
   if (Temp >= 0)
      outstr[0] = '+';
   else
      outstr[0] = '-';
   outstr[5] = '°';
   
   for(int i=0; i < 4; i++){
    outstr[i+1] = tempstr[i];
   }
//   else
//      sprintf (buf, "-%02i°", Temp);
//   Serial.println(buf);

   u8g2.drawStr(0,32,outstr);
   u8g2.sendBuffer(); 
}
